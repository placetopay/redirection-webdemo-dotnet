# Redirection Web Demo in ASP.NET Core 1.1 

## Requerimientos
- SDK Redirection for .Net Framework 4.5
- Newtonsoft.Json = v10.0.1
- SoapHttpClient = v1.4.3
- RestSharp = v105.2.3

## Usage

### Configuration

In Controllers/ParentController:
 
```csharp
//Gateway.TP_REST or Gateway.TP_SOAP
this.gateway = new P2P("YOR_LOGIN", "YOUR_TRANKEY", new Uri("YOUR_INTEGRATION_URL"), Gateway.TP_REST);
```
___

### Crear nueva sesión de pago
File: CreateRequestController.cs
 
#### Especificar:
- Monto a pagar "AMOUNT"
- Número único de referencia interno "REFERENCE"
- Descripción de pago "DESCRIPTION"
- Url de retorno "YOUR_URL"

```csharp
Amount amount = new Amount(AMOUNT);
Payment payment = new Payment("REFERENCE", "DESCRIPTION", amount);
String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
// Expiration time set in 30 minutes.
String expiration = (DateTime.Now).AddMinutes(30).ToString("yyyy-MM-ddTHH\\:mm\\:sszzz");
String returnUrl = "YOUR_URL";
// Capture IP ADDRESS
String ipAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
RedirectRequest request = new RedirectRequest(payment, returnUrl, ipAddress, userAgent, expiration);
RedirectResponse response = this.gateway.Request(request);
```
___

### Obtener información de una sesión de pago.
File: QueryController.cs
 
#### Especificar:
- Identificador de la sesión de pago "YOUR_REQUEST_ID"
 
```csharp
string requestId = "YOUR_REQUEST_ID";
RedirectInformation response = this.gateway.Query(requestId);
```
___

### Cobro sin intervención del usuario
File: CollectController.cs

#### Especificar:
- Token "YOUR_TOKEN"
- Datos del pagador: name, surname, document, document_type, email.
- Monto a pagar "AMOUNT"
- Identificador de referencia interna "REFERENCE"
- Descripción del pago "DESCRIPTION"
 
```csharp
String tokenStr = "YOUR_TOKEN";
Token token = new Token(tokenStr);
Instrument instrument = new Instrument(token);
// PAYER
string document = "PAYER_DOCUMENT";
Person person = new Person(document, "DOCUMENT_TYPE", "NAME", "SURNAME", "EMAIL");
Amount amount = new Amount(AMOUNT);
Payment payment = new Payment("REFERENCE", "DESCRIPTION", amount);
CollectRequest collectRequest = new CollectRequest(person, payment, instrument);
RedirectInformation collect = this.gateway.Collect(collectRequest);
```
___

### Reversar un pago
File: ReverseController.cs
 
#### Especificar:
- Identificador interno de pago "INTERNAL_REFERENCE"
 
```csharp
String internalReference = "INTERNAL_REFERENCE";
ReverseResponse response = this.gateway.Reverse(internalReference);
```