﻿using Microsoft.AspNetCore.Mvc;
using PlacetoPay.Integrations.Library.CSharp.Contracts;
using System;
using P2P = PlacetoPay.Integrations.Library.CSharp.PlacetoPay;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebDemo.Controllers
{
    public class ParentController : Controller
    {
        public Gateway gateway;

        public ParentController()
        {
            // Gateway.TP_REST or Gateway.TP_SOAP
            this.gateway = new P2P("YOR_LOGIN", "YOUR_TRANKEY", new Uri("YOUR_INTEGRATION_URL"), Gateway.TP_REST);
        }

    }
}
