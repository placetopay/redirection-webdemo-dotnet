﻿using Microsoft.AspNetCore.Mvc;
using PlacetoPay.Integrations.Library.CSharp.Message;
using PlacetoPay.Integrations.Library.CSharp.Serializer;
using System;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebDemo.Controllers
{
    public class ReverseController : ParentController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            String internalReference = "INTERNAL_REFERENCE";

            ReverseResponse response = this.gateway.Reverse(internalReference);

            ViewData["request"] = JsonSerializer.SerializeObject(internalReference);
            ViewData["response"] = JsonSerializer.SerializeObject(response);

            return View();
        }
    }
}
