﻿using Microsoft.AspNetCore.Mvc;
using PlacetoPay.Integrations.Library.CSharp.Message;
using PlacetoPay.Integrations.Library.CSharp.Serializer;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebDemo.Controllers
{
    public class QueryController : ParentController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            string requestId = "YOUR_REQUEST_ID";
            RedirectInformation response = this.gateway.Query(requestId);

            ViewData["request"] = JsonSerializer.SerializeObject(requestId);
            ViewData["response"] = JsonSerializer.SerializeObject(response);

            return View();
        }
    }
}
