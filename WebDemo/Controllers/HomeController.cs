﻿using Microsoft.AspNetCore.Mvc;

namespace WebDemo.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

    }
}
