﻿using Microsoft.AspNetCore.Mvc;
using PlacetoPay.Integrations.Library.CSharp.Entities;
using PlacetoPay.Integrations.Library.CSharp.Message;
using PlacetoPay.Integrations.Library.CSharp.Serializer;
using System;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebDemo.Controllers
{
    public class CollectController : ParentController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            String tokenStr = "YOUR_TOKEN";

            Token token = new Token(tokenStr);
            Instrument instrument = new Instrument(token);
            // PAYER
            int document = "PAYER_DOCUMENT";
            Person person = new Person(document, "DOCUMENT_TYPE", "NAME", "SURNAME", "EMAIL");
            Amount amount = new Amount(1000);
            Payment payment = new Payment("REFERENCE", "DESCRIPTION", amount);
            CollectRequest collectRequest = new CollectRequest(person, payment, instrument);
            RedirectInformation collect = this.gateway.Collect(collectRequest);

            ViewData["request"] = JsonSerializer.SerializeObject(collectRequest);
            ViewData["response"] = JsonSerializer.SerializeObject(collect);

            return View();
        }
    }
}
