﻿using Microsoft.AspNetCore.Mvc;
using PlacetoPay.Integrations.Library.CSharp.Entities;
using PlacetoPay.Integrations.Library.CSharp.Message;
using PlacetoPay.Integrations.Library.CSharp.Serializer;
using System;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebDemo.Controllers
{
    public class CreateRequestController : ParentController
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            // Specify the amount to be paid.
            Amount amount = new Amount(1000);
            Payment payment = new Payment("REFERENCE", "DESCRIPTION", amount);
            String userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/51.0.2704.103 Safari/537.36";
            // Expiration time set in 30 minutes.
            String expiration = (DateTime.Now).AddMinutes(30).ToString("yyyy-MM-ddTHH\\:mm\\:sszzz");
            String returnUrl = "YOUR_URL";
            // Capture IP ADDRESS
            String ipAddress = Request.HttpContext.Connection.RemoteIpAddress.ToString();
            RedirectRequest request = new RedirectRequest(payment, returnUrl, ipAddress, userAgent, expiration);
            RedirectResponse response = this.gateway.Request(request);

            ViewData["request"] = JsonSerializer.SerializeObject(request);
            ViewData["response"] = JsonSerializer.SerializeObject(response);

            return View();
        }
    }
}
